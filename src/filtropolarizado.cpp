#include "filtropolarizado.hpp"
#include <fstream>
#include <iostream>
#define PIXEL 1

using namespace std;

FiltroPolarizado::FiltroPolarizado(){
	
}
void FiltroPolarizado::aplicafiltro(char *nome, int intensidade, int largura, int altura, unsigned char ** matrizR, unsigned char ** matrizG, unsigned char ** matrizB){
	fstream ArquivoDestino;

	ArquivoDestino.open(nome, ios::binary | ios::app | ios::out);
	
	for(int i=0; i<altura; i++){
			for(int j=0; j<largura; j++){
					///RED
				if((int)matrizR[i][j]>(intensidade/2)){matrizR[i][j] = (unsigned char)intensidade;}
				else{matrizR[i][j] = 0;}
				ArquivoDestino.write((char*)&matrizR[i][j], sizeof(unsigned char));
				
					///GREEN
				if((int)matrizG[i][j]>(intensidade/2)){matrizG[i][j] = (unsigned char)intensidade;}
				else{matrizG[i][j] = 0;}
				ArquivoDestino.write((char*)&matrizG[i][j], sizeof(unsigned char));
		
					///BLUE	
				if((int)matrizB[i][j]>(intensidade/2)){matrizB[i][j] = (unsigned char)intensidade;}
				else{matrizB[i][j] = 0;}
				ArquivoDestino.write((char*)&matrizB[i][j], sizeof(unsigned char));
			}
		}
	

	
	ArquivoDestino.close();
}

