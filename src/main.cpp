#include <fstream>
#include "imagem.hpp"
#include "filtro.hpp"
#include "leitura.hpp"
#include "filtronegativo.hpp"
#include "filtropretoebranco.hpp"
#include "filtropolarizado.hpp"
#include "filtromedia.hpp"
#include "excecao.hpp"

using namespace std;


int main(){

	try{
		
			Imagem teste; // Creation of the image object. It's constructor method claims the name of the file to be modificated
					      // Cração do objeto imagem. O método construtor solicita o nome do arquivo a ser modificado

		// The Image class constructor reads the original file, copies the pixels to 3 matrixes (one for each color channel) and creates the result file containing only the header of the image
		// O construtor da classe Imagem lê o arquivo original, copia os pixels para 3 matrizes (uma para cada canal de cor) e cria o arquivo resultado contendo apenas o cabeçalho da imagem 



		

		

		int altura, largura, intensidade; //Translation: altura = height, largura = width, intensidade = intensity

		altura = teste.get_altura();
		largura = teste.get_largura();
		intensidade = teste.get_intensidade();

		char* pnome; // Pointer to the modified file's name
					 // Ponteiro para o nome do arquivo modificado
		pnome = teste.get_nome();
		
		unsigned char ** matrizR; // Pointer to the red channel matrix
								  // Ponteiro para a matriz do canal vermelho
		matrizR = teste.get_matrizR();
		
		
		unsigned char ** matrizG; // Pointer to the green channel matrix
								 // Ponteiro para a matriz do canal verde
		matrizG = teste.get_matrizG();
		
		
		unsigned char ** matrizB; //Pointer to the blue channel matrix
								  // Ponteiro para a matriz do canal azul
		matrizB = teste.get_matrizB();


		cout << "Menu de filtros para a imagem \"" << pnome << "\":" << endl << "1 - Filtro Negativo\n2 - Fitro Polarizado\n3 - Filtro Preto e Branco\n4 - Filtro de Media" << endl;
		// Translation: "Image Filter Menu", "1 - Negative Filter", "2 - Polarized Filter", "3 - Grayscale Filter", "4 - Blur Filter (Average Filter)"
		
		int opcao; // Translation: opcao = option 
		do{opcao = le_teclado_int(); //The function 'le_teclado_int()' belongs to "leitura.hpp" and it safely reads an integer from cin
			if(opcao > 4 || opcao <1 ){
				cout << "Insira uma das opções acima"<<endl; //Translation: "Insert one of the options above"
			}

		}while(opcao > 4 || opcao < 1);
		FiltroNegativo FiltroNeg; // Inicialization of the Negative Filter object
								  // Inicialização do objeto Filtro Negativo
		
		FiltroPretoeBranco FiltroPB; // Inicialization of the Grayscale Filter object
									 // Inicialização do objeto Filtro Preto e Branco
		
		FiltroPolarizado FiltroP; // Inicialization of the Polarized Filter object
								  // Inicialização do objeto Filtro Polarizado
		FiltroMedia FiltroM; // Inicialization of the Blur Filter(Average Filter) object
							 // Inicialização do objeto Filtro de Média

		// All of the filter subclasses inherit from the main filter class the funcion 'aplicafiltro' which applies the filter to the image
		// Todas as subclasses filtro herdam da classe filtro a função 'aplicafiltro' que aplica o filtro na imagem

		switch(opcao){
			case 1:
			FiltroNeg.aplicafiltro(pnome, intensidade, largura, altura, matrizR, matrizG, matrizB);
			cout << "Filtro Negativo aplicado com sucesso!"<<endl;
			break;
			case 2:
			FiltroP.aplicafiltro(pnome, intensidade, largura, altura, matrizR, matrizG, matrizB);
			cout << "Filtro Polarizado aplicado com sucesso!"<<endl;
			break;
			case 3:
			FiltroPB.aplicafiltro(pnome, intensidade, largura, altura, matrizR, matrizG, matrizB);
			cout << "Filtro Preto e Branco aplicado com sucesso!"<<endl;
			break;
			case 4:
			FiltroM.aplicafiltro(pnome, largura, altura, matrizR, matrizG, matrizB);
			cout << "Filtro de Média aplicado com sucesso!"<<endl;
			break;
		}
	}
	catch(Excecao &e){
		//Caso exista uma exceção do tipo Excecao, será encerrado o programa
		//In case of exceoption of type "Excecao" the process will be terminated
	cout << e.what()<<endl;
	cout << "Finalizando Programa..."<<endl; // Translation: "Terminating process..."      
	return 0;
	}

	return 0;
}

