#include "filtropretoebranco.hpp"
#include <fstream>
#include <iostream>
#define PIXEL 1

using namespace std;

FiltroPretoeBranco::FiltroPretoeBranco(){
	
}
void FiltroPretoeBranco::aplicafiltro(char *nome, int intensidade, int largura, int altura, unsigned char ** matrizR, unsigned char ** matrizG, unsigned char ** matrizB){
	fstream ArquivoDestino;


	ArquivoDestino.open(nome, ios::binary | ios::app | ios::out);
	long int pretoebranco;
	
	for(int i=0; i<altura; i++){
			for(int j=0; j<largura; j++){
				pretoebranco = ((0.299 * matrizR[i][j]) + (0.587 * matrizG[i][j]) + (0.144 * matrizB[i][j]));
				if(pretoebranco>intensidade){pretoebranco = intensidade;}
				matrizR[i][j] = (unsigned char)pretoebranco;	
				ArquivoDestino.write((char*)&matrizR[i][j], sizeof(unsigned char));
				
				matrizG[i][j] = (unsigned char)pretoebranco;
				ArquivoDestino.write((char*)&matrizG[i][j], sizeof(unsigned char));
		
				matrizB[i][j] = (unsigned char)pretoebranco;
				ArquivoDestino.write((char*)&matrizB[i][j], sizeof(unsigned char));
			}
		}


	
	ArquivoDestino.close();
}



