#include "filtronegativo.hpp"
#include <fstream>
#include <iostream>

using namespace std;

FiltroNegativo::FiltroNegativo(){
	
}
void FiltroNegativo::aplicafiltro(char *nome, int intensidade, int largura, int altura, unsigned char** matrizR, unsigned char** matrizG, unsigned char** matrizB){
	fstream ArquivoDestino;

	ArquivoDestino.open(nome, ios::binary | ios::app | ios::out);
	
	
		for(int i=0; i<altura; i++){
			for(int j=0; j<largura; j++){ 
				matrizR[i][j] = intensidade - (int)matrizR[i][j];	
				ArquivoDestino.write((char*)&matrizR[i][j], sizeof(unsigned char));
				

				matrizG[i][j] = intensidade - (int)matrizG[i][j];
				ArquivoDestino.write((char*)&matrizG[i][j], sizeof(unsigned char));
				
		
				matrizB[i][j] = intensidade - (int)matrizB[i][j];
				ArquivoDestino.write((char*)&matrizB[i][j], sizeof(unsigned char));
				
			}
		}

	ArquivoDestino.close();
}