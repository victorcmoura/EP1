#include "filtromedia.hpp"
#include <fstream>
#include <iostream>

using namespace std;

FiltroMedia::FiltroMedia(){
	
}
void FiltroMedia::aplicafiltro(char *nome, int largura, int altura, unsigned char ** matrizR, unsigned char ** matrizG, unsigned char ** matrizB){
	fstream ArquivoDestino;

	int tamanhofiltro;
	cout << "Insira o tamanho do filtro" << endl;
	do{cin >> tamanhofiltro;if(tamanhofiltro < 0){cout << "Insira valor positivo"<<endl;}}while(tamanhofiltro < 0);
	ArquivoDestino.open(nome, ios::binary | ios::app | ios::out);
	
	int auxiliarR = 0, auxiliarG = 0, auxiliarB = 0, contador = 0;

	tamanhofiltro = tamanhofiltro/2;

	for(int x=0; x<altura; x++){
		for(int y=0; y<largura; y++){
			
			for(int a=-tamanhofiltro; a<=tamanhofiltro; a++){
				for(int b=-tamanhofiltro; b<=tamanhofiltro; b++){

					if((x+a) >= 0 && (a+x) < altura){
						if((y+b)>=0 && (y+b) < largura){
							auxiliarR += (int)matrizR[x+a][y+b];
							auxiliarG += (int)matrizG[x+a][y+b];
							auxiliarB += (int)matrizB[x+a][y+b];
							contador++;
						}
					}

				}
			}
			auxiliarR = auxiliarR/contador;
			auxiliarG = auxiliarG/contador;
			auxiliarB = auxiliarB/contador;
			matrizR[x][y] = (unsigned char)auxiliarR;
			matrizG[x][y] = (unsigned char)auxiliarG;
			matrizB[x][y] = (unsigned char)auxiliarB;

			ArquivoDestino.write((char*)&matrizR[x][y], sizeof(unsigned char));
			ArquivoDestino.write((char*)&matrizG[x][y], sizeof(unsigned char));
			ArquivoDestino.write((char*)&matrizB[x][y], sizeof(unsigned char));

			auxiliarR = 0;
			auxiliarB = 0;
			auxiliarG = 0;
			contador = 0; 


		}
	}

	ArquivoDestino.close();
}