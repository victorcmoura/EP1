#include "imagem.hpp"
#include <fstream>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include "excecao.hpp"
using namespace std;

Imagem::Imagem(){
char NomeDoArquivo[100], NovoArquivo[100];
int largura, altura, intensidade;


cout << "Digite o nome do arquivo origem: "; //Translation: "Insert the original file's name"
cin >> NomeDoArquivo; 


strcpy(NovoArquivo, "filtrado_"); //Translation: "filtrado" = "filtered"
strcpy(nomebase, NomeDoArquivo);

fstream arquivoOrigem;

arquivoOrigem.open(NomeDoArquivo);

char charaux1[200];
string auxs;
getline(arquivoOrigem, auxs);



if(auxs == "P6"){

for(int x = 0; x<3; x=x){

long inicio;
inicio = arquivoOrigem.tellp();
arquivoOrigem >> charaux1;
if(charaux1[0]!='#'){

arquivoOrigem.seekg(inicio);

switch(x){

	case 0:
	arquivoOrigem >> largura;
	this->largura = largura;
	break;
	case 1:
	arquivoOrigem >> altura;
	this->altura = altura;
	break;
	case 2:
	arquivoOrigem >> intensidade;
	this->intensidade = intensidade;
	inicio = arquivoOrigem.tellp();
	inicio += 1; ///É somado 1 para pular a quebra de linha presente após a leitura da intensidade
	this->inicio = inicio;
	break;
}


x++;
}
else{getline(arquivoOrigem, auxs);}
}


arquivoOrigem.close();
arquivoOrigem.open(NomeDoArquivo, ios::binary | ios::in);
arquivoOrigem.seekg(inicio); 

matrizR = (unsigned char**)malloc(altura * sizeof(unsigned char*));
matrizG = (unsigned char**)malloc(altura * sizeof(unsigned char*)); 
matrizB = (unsigned char**)malloc(altura * sizeof(unsigned char*)); 

for (int i = 0; i < altura; i++){

     matrizR[i] = (unsigned char*) malloc(largura * sizeof(unsigned char));
     matrizG[i] = (unsigned char*) malloc(largura * sizeof(unsigned char));
     matrizB[i] = (unsigned char*) malloc(largura * sizeof(unsigned char));
    
     for (int j = 0; j < largura; j++){ 

        arquivoOrigem.read((char*)&matrizR[i][j], 1);
		arquivoOrigem.read((char*)&matrizG[i][j], 1);
		arquivoOrigem.read((char*)&matrizB[i][j], 1);

     }
}


ofstream nArquivo;

strcat(NovoArquivo, NomeDoArquivo);

nArquivo.open(NovoArquivo);
nArquivo << "P6" << endl << largura << " "<< altura << endl << intensidade << endl;

cout << "Arquivo cabeçalho criado com sucesso!" << endl; //Translation: "Header file successfully created"
strcpy(nome, NovoArquivo);
nArquivo.close();
}
else{throw Excecao("Erro: Arquivo em Formato Inválido!");} // In case of nonexitant file, the constructor method will throw an Excecao type exception 
														   // No caso de arquivo inexistente, o método construtor apresentará um caso de exceção do tipo Excecao
														   // Translation: "Error: Invalid file type!"

arquivoOrigem.close();
}


	int Imagem::get_altura(){return altura;}
	void Imagem::set_altura(int altura){this->altura = altura;}

	int Imagem::get_intensidade(){return intensidade;}
	void Imagem::set_intensidade(int intensidade){this->intensidade = intensidade;}

	int Imagem::get_largura(){return largura;}
	void Imagem::set_largura(int largura){this->largura = largura;}

	long Imagem::get_inicio(){return inicio;}
	void Imagem::set_inicio(long inicio){this->inicio = inicio;}

	char* Imagem::get_nome(){return nome;}

	char* Imagem::get_nomebase(){return nomebase;}

	unsigned char** Imagem::get_matrizR(){return matrizR;}
	unsigned char** Imagem::get_matrizG(){return matrizG;}
	unsigned char** Imagem::get_matrizB(){return matrizB;}

