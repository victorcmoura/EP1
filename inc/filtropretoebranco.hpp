#ifndef FILTROPRETOEBRANCO_HPP
#define FILTROPRETOEBRANCO_HPP
#include "filtro.hpp"
#include <iostream>
using namespace std;
class FiltroPretoeBranco : public Filtro{
	public:
		FiltroPretoeBranco();
		void aplicafiltro(char *nome, int intensidade, int largura, int altura, unsigned char ** matrizR, unsigned char ** matrizG, unsigned char ** matrizB);
};

#endif