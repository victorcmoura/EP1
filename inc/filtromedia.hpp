#ifndef FILTROMEDIA_HPP
#define FILTROMEDIA_HPP
#include "filtro.hpp"
#include <iostream>

using namespace std;
class FiltroMedia : public Filtro{
	public:
		FiltroMedia();
		void aplicafiltro(char *nome, int largura, int altura, unsigned char ** matrizR, unsigned char ** matrizG, unsigned char ** matrizB);
};

#endif