#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP
#include "filtro.hpp"
#include <iostream>
using namespace std;
class FiltroPolarizado : public Filtro{
	public:
		FiltroPolarizado();
		void aplicafiltro(char *nome, int intensidade, int largura, int altura, unsigned char ** matrizR, unsigned char ** matrizG, unsigned char ** matrizB);
};

#endif