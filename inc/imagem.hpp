#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>

using namespace std;

class Imagem{

private:
	int altura;
	int intensidade;
	int largura;
	long inicio;
	char nome[50];
	char nomebase[50];
	unsigned char **matrizR;
	unsigned char **matrizG;
	unsigned char **matrizB;

public:
	Imagem(); ///Metodo construtor padrão

	int get_altura();
	void set_altura(int altura);

	int get_intensidade();
	void set_intensidade(int intensidade);

	int get_largura();
	void set_largura(int largura);

	long get_inicio();
	void set_inicio(long inicio);

	char* get_nome();
	char* get_nomebase();

	unsigned char ** get_matrizR();
	unsigned char ** get_matrizG();
	unsigned char ** get_matrizB();
};

#endif