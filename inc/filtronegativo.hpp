#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP
#include "filtro.hpp"
#include <iostream>
using namespace std;
class FiltroNegativo : public Filtro{
	public:
		FiltroNegativo();
		void aplicafiltro(char *nome, int intensidade, int largura, int altura, unsigned char** matrizR, unsigned char** matrizG, unsigned char** matrizB);
};

#endif