# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

Como usar o programa:

	- Inserir os arquivos .ppm no mesmo diretório em que este readme se encontra.
	- Execução do programa:
		- Será solicitado o nome do arquivo no formato "<nome>.ppm"

		- Inserir a opção de filtro desejado.
			- Filtro de Média:
				- Inserir valor da intensidade do blur gerado na imagem (inteiro positivo). Para escolher o filtro de média com máscara 3x3, por exemplo, insira 3 como intensidade do blur.

		- Será gerada uma nova imagem com o nome "filtrado_<>.ppm" localizada no mesmo diretório da imagem original.




